const app = require('express')();
const user = require('../model/users');
const userRoles = require('../model/userRoles');
const bcrypt = require('bcrypt');
const mongoose = require("mongoose");

module.exports.register = async (request, response) => {
    //convert userType to lower Case
    var userType = request.body.userType.toLowerCase().trim();
    var id = new mongoose.Types.ObjectId();

    //search by 'admin' 
    const adminExist = await user.findOne({ userType });

    //check if Admin Exist
    if (adminExist.userType === 'admin')
    //return and stop further process
        return response.status(401).send('Your not authorised to be Admin \nPlease try with different user type!');

    //Hash passwords
    const hashPassword = await bcrypt.hash(request.body.password, 8);

    //get userData
    const userData = new user({
        _id: id,
        fName: request.body.fName,
        lName: request.body.lName,
        email: request.body.email,
        password: hashPassword,
        userType
    });

    const userRolesData = new userRoles({
        _id: id,
        role: userType
    });

    try {
        const saveData = await userData.save();
        try {
            const saveUserRoles = await userRolesData.save();
            //user created
            response.status(201).send('User created');
        } catch (error) {
            console.log(error.message);
            response.status(400);
        }
    } catch (error) {
        console.log(error.message);
        response.status(400);
    }
}