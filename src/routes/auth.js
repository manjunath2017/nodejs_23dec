const router = require('express').Router();
const ctrlRegister = require('../controllers/register');

// localhost:5000/api/user/register
router.post('/register', ctrlRegister.register);

module.exports = router;