//mongoose ORM
const mongoose = require('mongoose');
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

//import secret code
require('dotenv').config();

//DB connection
mongoose.connect(process.env.DB_CONNECTION,  (error)=>{
    if(!error) 
        return console.log(' Db connected!!! \n');
    console.log(' DB error! ');
});
