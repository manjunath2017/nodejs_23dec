const mongoose = require('mongoose');
//DataBase structure
const userRoles = mongoose.Schema({
    _id:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'users', 
        required: true
    },
    role:{
        type:String,
        require:true
    }
});
// Register model and we can access it in any files
// 'userroles' is a collection name
// Define 'userRoles'
module.exports = mongoose.model('userroles', userRoles);