const mongoose = require('mongoose');
//DataBase structure
const user = mongoose.Schema({
    _id:mongoose.Types.ObjectId,
    fName:{
        type:String,
        require:true
    },
    lName:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    date:{
        type:Date,
        default:Date.now
    },
    userType:{
        type:String,
        require:true,
        trim: true
    }
});
// Registering model and we can have access across all files(module.exports)
// 'newuser' is a collection name
// Define 'user'
module.exports = mongoose.model('newusers', user);