//express framework
const app = require('express')();

//hide your secret code in .env file
require('dotenv').config();

//this helps during post method
const bodyParser = require('body-parser')
app.use(bodyParser.json());

//DB connection
require('./src/db/dbConnection');

//user router
const router = require('./src/routes/auth');
app.use('/api/user', router);

app.listen(process.env.PORT || 3000, (error)=>{
    if(!error) 
        return console.log(`server running on port ${process.env.PORT || 3000}`)
    console.log(`server error `)    
});
